package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Chronometer.OnChronometerTickListener;

public class TapNumber extends Activity{

	ImageView quespic,gong;
	TextView scoreshow,time;
	Button ans2;
	
	MediaPlayer bgmusic; 
	SoundPool tong;
	int score=0;
	int i=1;
	int counter =0;
	int random=1;
	long startTime;
    long countUp;
	
    private SoundPool mSoundPool;
    private AudioManager  mAudioManager;
    private HashMap<Integer, Integer> mSoundPoolMap;
    private int mStream1 = 0;
    final static int SOUND_FX_01 = 1;
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tap);
		
		//set up our audio player

		mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		mSoundPoolMap = new HashMap();
		
		mSoundPoolMap.put(SOUND_FX_01, mSoundPool.load(this, R.raw.gong, 1));
		
		int id2 = playmusic();
		
		bgmusic = MediaPlayer.create(TapNumber.this, id2);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
		
		
		Chronometer stopWatch = (Chronometer) findViewById(R.id.chrono);
        startTime = SystemClock.elapsedRealtime();
        
        scoreshow = (TextView) findViewById(R.id.score);
        time = (TextView) findViewById(R.id.timer);
        
        stopWatch.setOnChronometerTickListener(new OnChronometerTickListener(){
            public void onChronometerTick(Chronometer arg0) {
                countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                String asText = (countUp / 60) + ":" + (countUp % 60); 
                time.setText(asText);
            }
        });
        stopWatch.start();
        
		addListenerOnButton();
		
	}
	
	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		if(i<15)
		{
		counter = 0;	
		
		quespic = (ImageView) findViewById(R.id.ques);
		scoreshow = (TextView) findViewById(R.id.score);
		gong = (ImageView) findViewById(R.id.gong);
		
		random = randomnum();
		int col = quesload(random);
		
		quespic.setBackgroundResource(col);
		
		ans2 = (Button) findViewById(R.id.answer2);
				
		gong.setOnTouchListener(new View.OnTouchListener() {
			
		//	public void onTouch(View v) {
				// TODO Auto-generated method stub
		
	//		}

			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

				streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			
			mStream1= mSoundPool.play(mSoundPoolMap.get(SOUND_FX_01), streamVolume, streamVolume, 1, 0, 1f);
				
				counter++;
				return false;
			}
		});
		
		ans2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					//userans = ans;
				if(counter==random)
				{
					score++;
				}
				
				else
		    	{
		    	stopquiz();
		    	}
		    
		    scoreshow.setText(String.valueOf(score));
		    i++;
		    addListenerOnButton();
			}
		});
		
			
		}
		
		else {stopquiz();}
	}
	
	public int quesload(int i)
	{
		int imgId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("mem_"+i);
		    imgId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return imgId;
	}
	
	public void stopquiz()
	{
		String scorefinal=Integer.toString(score);
		scorefinal = scorefinal + "/15";
		String timefinal = String.valueOf(time.getText());
		int idgame = 3;
		
		Intent i = new Intent(TapNumber.this,ScoreExer.class);
		i.putExtra("score1", scorefinal);
		i.putExtra("idgame", idgame);
		i.putExtra("time1", timefinal);
		startActivity(i);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
		
	public int randomnum()
	{
	List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
													//
	int max=10,n=1;									//
	for(int i = 1; i<=max; i++)						//
	    list.add(i);								//
													//
	Collections.shuffle(list);						//
													//
	for(int i=0; i<max; i++) {						//			
	     n = list.get(i);							//	
	}
	return n;
	}
	
		public int playmusic()
		{
		List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
														//
		int max=14,n=1;									//
		for(int i = 1; i<=max; i++)						//
		    list.add(i);								//
														//
		Collections.shuffle(list);						//
														//
		for(int i=0; i<max; i++) {						//			
		     n = list.get(i);							//	
		}												//
		
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("bg_"+n);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
		
	}
}