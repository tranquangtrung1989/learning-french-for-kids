package com.lolohardy.lkfk;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	//called when the activity is first created//

	ImageButton num,col,ani,bp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		
		addListenerOnButton();
	}

	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		
		num = (ImageButton) findViewById(R.id.imgButtonNum);
		col = (ImageButton) findViewById(R.id.imgButtonCol);
		ani = (ImageButton) findViewById(R.id.imgButtonAni);
		bp = (ImageButton) findViewById(R.id.imgButtonBP);
		
		num.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openSelectNum = new Intent("com.lolohardy.lkfk.SELECTNUM");
				startActivity(openSelectNum);
			}
		});
		
		col.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openSelectCol = new Intent("com.lolohardy.lkfk.SELECTCOL");
				startActivity(openSelectCol);
			}
		});
		
		ani.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openSelectAni = new Intent("com.lolohardy.lkfk.SELECTANI");
				startActivity(openSelectAni);
			}
		});
		
		bp.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openSelectPhr = new Intent("com.lolohardy.lkfk.SELECTPHR");
				startActivity(openSelectPhr);
			}
		});
	}
	
	
}
