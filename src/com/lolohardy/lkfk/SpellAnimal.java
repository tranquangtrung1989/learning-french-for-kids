package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Chronometer.OnChronometerTickListener;

public class SpellAnimal extends Activity{
	
	ImageView quespic;
	TextView scoreshow,time,hint2;
	EditText spellans;
	TextWatcher w;
	Button done;
	MediaPlayer bgmusic;
	String m="";
	int score=0;
	int i=1;
	long startTime;
    long countUp;
	
    ///////////answer///////////
    String ans[] = {"sapi","tasu","putik","sada","kambing","ansa","kuda","vogok","biri biri"
    				,"buhanut","bosing","buu","pondu","tombohog","kalabau","tingau", "manuk"};
    /////////////hint////////

    //////////////////////////////
	   
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ani_sir);
		
		int id2 = playmusic();
		
		bgmusic = MediaPlayer.create(SpellAnimal.this, id2);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
		
		spellans = (EditText) findViewById(R.id.answer_sir);
		scoreshow = (TextView) findViewById(R.id.score);
		hint2 = (TextView) findViewById(R.id.hint2);
		done = (Button) findViewById(R.id.enter);
		
		Chronometer stopWatch = (Chronometer) findViewById(R.id.chrono);
        startTime = SystemClock.elapsedRealtime();
        
        time = (TextView) findViewById(R.id.timer);
        
        stopWatch.setOnChronometerTickListener(new OnChronometerTickListener(){
            public void onChronometerTick(Chronometer arg0) {
                countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                String asText = (countUp / 60) + ":" + (countUp % 60); 
                time.setText(asText);
            }
        });
        stopWatch.start();
        
		addListenerOnButton();
		
	}
	
	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		if(i<18){
		quespic = (ImageView) findViewById(R.id.ques);
		spellans.setText("");
		
				
				
		int col = quesload(i);
		quespic.setBackgroundResource(col);
		String answ="";
		for(int c=0;c<=i;c++)
		{
			answ=ans[i-1];
		}
		//display hint
		
		char [] result = new char[15];
		answ = "Hint : "+answ;
		
		answ.getChars(0, 10, result, 0);
		
		for(int c=10;c<14;c++)
		{
			result[c] = '.';
		}
		
		hint2.setText(result, 0, 12);
		
		m = ans[i-1];
			
		done.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			
				if(spellans.getText().toString().equalsIgnoreCase(m))
				{
					Toast.makeText(SpellAnimal.this,
					"Correct!", Toast.LENGTH_SHORT).show();
					score++;
				}	
				
				i++;
				scoreshow.setText(String.valueOf(score));
				addListenerOnButton();
			}
		});
		
		}
		
		else
		{stopquiz();}
	}
	
	public int quesload(int i)
	{
		int imgId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("spell_"+i);
		    imgId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return imgId;
	}
	
	public int hintload(int i)
	{
		int hintId = 0;
		try {
		    Class res = R.class;
		    Field field = res.getField("hint_"+i);
		    hintId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return hintId;
	}
	
	
	public void stopquiz()
	{
		String scorefinal=Integer.toString(score);
		scorefinal = scorefinal + "/17";
		String timefinal = String.valueOf(time.getText());
		int idgame = 2;
		
		Intent i = new Intent(SpellAnimal.this,ScoreExer.class);
		i.putExtra("score1", scorefinal);
		i.putExtra("idgame", idgame);
		i.putExtra("time1", timefinal);
		startActivity(i);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
		
		public int playmusic()
		{
		List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
														//
		int max=14,n=1;									//
		for(int i = 1; i<=max; i++)						//
		    list.add(i);								//
														//
		Collections.shuffle(list);						//
														//
		for(int i=0; i<max; i++) {						//			
		     n = list.get(i);							//	
		}												//
		
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("bg_"+n);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
		
	}
}
