package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ColourQuiz extends Activity {

	String butans1,butans2,butans3; //answer choices
	String choice[] = {"Noir","bleu","Vert","griz","Rouge","Blanc","Jaune"};
	ImageView quespic;
	TextView scoreshow,time;
	Button ans1,ans2,ans3;
	MediaPlayer bgmusic;
	int score=0;
	int i=1;
	long startTime;
    long countUp;
	
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.color_gtc);
		
		int id2 = playmusic();
		
		bgmusic = MediaPlayer.create(ColourQuiz.this, id2);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
		
		Chronometer stopWatch = (Chronometer) findViewById(R.id.chrono);
        startTime = SystemClock.elapsedRealtime();
        
        scoreshow = (TextView) findViewById(R.id.score);
        time = (TextView) findViewById(R.id.timer);
        
        stopWatch.setOnChronometerTickListener(new OnChronometerTickListener(){
            public void onChronometerTick(Chronometer arg0) {
                countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                String asText = (countUp / 60) + ":" + (countUp % 60); 
                time.setText(asText);
            }
        });
        stopWatch.start();
        
		addListenerOnButton();
		
	}
	
	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		if(i<8)
		{
			
		
		quespic = (ImageView) findViewById(R.id.ques);
		scoreshow = (TextView) findViewById(R.id.score);

		int col = quesload(i);
		
		quespic.setBackgroundResource(col);
		
		
		ans1 = (Button) findViewById(R.id.answer1);
		ans2 = (Button) findViewById(R.id.answer2);
		ans3 = (Button) findViewById(R.id.answer3);
		
		if(i%2==0 && i<= 4)
		{
			//ques 2/4
			ans1.setText(choice[i-1]);
			ans2.setText(choice[i-2]);
			ans3.setText(choice[i]);
		}
		
		else if(i%3==0)
		{
			//ques 3
			ans1.setText(choice[i-2]);
			ans2.setText(choice[i-1]);
			ans3.setText(choice[6]);			
		}
		
		else
		{
			//ques 1,5,7
			ans1.setText(choice[2]);
			ans2.setText(choice[5]);
			ans3.setText(choice[i-1]);
		}
		
		
		
		ans1.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					//userans = ans;
			    butans1 = ((ans1.getText()).toString());
			  
			    if(butans1.equals(choice[i-1]))
			    	{
			    	score++;
			    	}
			    
			    scoreshow.setText(String.valueOf(score));
			    i++;
			    addListenerOnButton();
			}
		});
		
		ans2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					//userans = ans;
				butans2 = ((ans2.getText()).toString());
				
				
			    if(butans2.equals(choice[i-1]))
		    	{
		    	score++;
		    	}
		    
		    scoreshow.setText(String.valueOf(score));
		    i++;
		    addListenerOnButton();
			}
		});
		
		ans3.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					//userans = ans;
				butans3 = ((ans3.getText()).toString());
				
			    if(butans3.equals(choice[i-1]))
		    	{
		    	score++;
		    	}
		    
		   scoreshow.setText(String.valueOf(score));
		    i++;
		    addListenerOnButton();
			}
		});
		
		}
		
		else {stopquiz();}
	}
	
	public int quesload(int i)
	{
		int imgId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("quescol_"+i);
		    imgId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return imgId;
	}
	
	public void stopquiz()
	{
		String scorefinal=Integer.toString(score);
		scorefinal = scorefinal + "/7";
		String timefinal = String.valueOf(time.getText());
		int idgame = 1;
		
		Intent i = new Intent(ColourQuiz.this,ScoreExer.class);
		i.putExtra("score1", scorefinal);
		i.putExtra("idgame", idgame);
		i.putExtra("time1", timefinal);
		startActivity(i);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
		
		public int playmusic()
		{
		List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
														//
		int max=14,n=1;									//
		for(int i = 1; i<=max; i++)						//
		    list.add(i);								//
														//
		Collections.shuffle(list);						//
														//
		for(int i=0; i<max; i++) {						//			
		     n = list.get(i);							//	
		}												//
		
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("bg_"+n);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
		
	}
}