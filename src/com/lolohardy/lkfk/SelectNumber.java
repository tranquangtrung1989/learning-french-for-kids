package com.lolohardy.lkfk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class SelectNumber extends Activity{

ImageButton learn,exer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectnumber);
		
		addListenerOnButton();
	}

	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		
		learn = (ImageButton) findViewById(R.id.iBColLearn);
		exer = (ImageButton) findViewById(R.id.iBColExe);
		
		
		learn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openNumLesson = new Intent("com.lolohardy.lkfk.LESSONNUM");
				startActivity(openNumLesson);
			}
		});
		
		exer.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openNumExer = new Intent("com.lolohardy.lkfk.TAPNUM");
				startActivity(openNumExer);
			}
		});
	}
}
