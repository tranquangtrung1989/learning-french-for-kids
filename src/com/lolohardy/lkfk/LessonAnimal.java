package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import android.app.Activity;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LessonAnimal extends Activity {
	
	
	ImageButton prev, next, speak;
	ImageView ani;
	TextView word;
	MediaPlayer bgmusic,pronounce;
	//View colflash;
	int i=1;
	
	public void onWindowFocusChanged (boolean hasFocus) 
	{
		ani = (ImageView) findViewById(R.id.imgviewAni);
		
	    super.onWindowFocusChanged(hasFocus);
		ani.setBackgroundResource(R.drawable.ani_1);
		 AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
	    if(hasFocus) {
		       startAnimation.start();
	    } else {
	        startAnimation.stop();
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lesson_animal);
		
		int id = playmusic();
		bgmusic = MediaPlayer.create(LessonAnimal.this, id);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
	        
		word = (TextView) findViewById(R.id.imgviewWord);
		word.setText("Chien\nDog ");
		addListenerOnButton();
	
	}

	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		
		prev = (ImageButton) findViewById(R.id.btnprev);
		next = (ImageButton) findViewById(R.id.btnnext);
		speak = (ImageButton) findViewById(R.id.speakbut);
		
		prev.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(i==1)
				{
					i=17;
				}
				
				else
				{
					i-=1;
				}
				int col = prev(i);
				String col2 = prev2(i);
				ani.setBackgroundResource(col);
				word.setText(col2);
				AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
			       startAnimation.start();
			


			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
				if(i==17)
				{
					i=1;
				}
				else
				{
					i+=1;
				}
				int col = prev(i);
				String col2 = prev2(i);
				ani.setBackgroundResource(col);
				word.setText(col2);
				 AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
			       startAnimation.start();
			}
		});
		
		speak.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				int sound = sound(i);
			     	
			       pronounce = MediaPlayer.create(LessonAnimal.this, sound);
					pronounce.setVolume(100, 100);
			        pronounce.start();
			        while(pronounce.isPlaying())
			        	{bgmusic.pause();}
			        	bgmusic.start();
			        


			}
		});
	}

	public int prev(int i)
	{
		int drawableId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("ani_"+i);
		    drawableId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return drawableId;
	}
	
	public String prev2(int i)
	{
		String wordtopost = " ";
		if(i==1) wordtopost="Chien\nDog";
		if(i==2) wordtopost="Chat\nCat";
		if(i==3) wordtopost="Oiseau\nBird";
		if(i==4) wordtopost="bufle\nBuffalo";
		if(i==5) wordtopost="Poule\nChicken";
		if(i==6) wordtopost="Canard\nDuck";
		if(i==7) wordtopost="Poison\nFish";
		if(i==8) wordtopost="Serpan\nSnake";
		if(i==9) wordtopost="Shever\nGoat";
		if(i==10) wordtopost="Mouton\nSheep";
		if(i==11) wordtopost="Vach\nCow";
		if(i==12) wordtopost="Ecureuil\nSquirrel";
		if(i==13) wordtopost="Oie\nGoose";
		if(i==14) wordtopost="Torue\nTurtle";
		if(i==15) wordtopost="Tortu\nTortoise";
		if(i==16) wordtopost="Porc\nPig";
		if(i==17) wordtopost="Cheval\nHorse";
		
		return wordtopost;
	}
	
	public int sound(int i)
	{
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("ani_"+i);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
	
	public int playmusic()
	{
	List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
													//
	int max=14,n=1;									//
	for(int i = 1; i<=max; i++)						//
	    list.add(i);								//
													//
	Collections.shuffle(list);						//
													//
	for(int i=0; i<max; i++) {						//			
	     n = list.get(i);							//	
	}												//
	
	int rawId = 0;
	try {
	    Class res = R.raw.class;
	    Field field = res.getField("bg_"+n);
	    rawId = field.getInt(null);
	}
	catch (Exception e) {
	    Log.e("MyTag", "Failure to get drawable id.", e);
	}
	return rawId;
	
	}
}

