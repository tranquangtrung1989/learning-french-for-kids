package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Chronometer.OnChronometerTickListener;

public class PhraseIt extends Activity{
	ImageButton ans1,ans2;
	TextView scoreshow,time,ques;
		
	MediaPlayer bgmusic; 
	
	int score=0;
	int i=1, a=0, b=0, c=0;
	int counter =0;
	int random=1;
	long startTime;
    long countUp;
	
    String text[] ={
    				"Good Night",
    				"Hello",
    				"Good Afternoon",
    				"I'm Sorry",
    				"Thank You",
    				"Good Morning",
    				"Goodbye",
    				"Good Evening",
    				"Happy Birthday",
    				"You're Welcome"
    			  };
   
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.phraseit);
		
		//set up our audio player
		
		int id2 = playmusic();
		
		bgmusic = MediaPlayer.create(PhraseIt.this, id2);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
		
		
		Chronometer stopWatch = (Chronometer) findViewById(R.id.chrono);
        startTime = SystemClock.elapsedRealtime();
        
        scoreshow = (TextView) findViewById(R.id.score);
        time = (TextView) findViewById(R.id.timer);
        
        stopWatch.setOnChronometerTickListener(new OnChronometerTickListener(){
            public void onChronometerTick(Chronometer arg0) {
                countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                String asText = (countUp / 60) + ":" + (countUp % 60); 
                time.setText(asText);
            }
        });
        stopWatch.start();
        
		addListenerOnButton();
		
	}
	
	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		if(i<11)
		{
		counter = 0;	
		
		ques = (TextView) findViewById(R.id.question);
		ans1 = (ImageButton) findViewById(R.id.answer1);
		ans2 = (ImageButton) findViewById(R.id.answer2);
		
		ques.setText(text[i-1]);
		
		
			 if(i==1){a=6;b=4;} // 6    /    
		else if(i==2){b=1;a=9;} // 1     
		else if(i==3){a=4;b=7;} // 4    /
		else if(i==4){b=10;a=1;}// 10    
		else if(i==5){a=8;b=2;} // 8    /
		else if(i==6){a=3;b=5;} // 3    /
		else if(i==7){b=2;a=6;} // 2   
		else if(i==8){b=5;a=3;} // 5
		else if(i==9){a=7;b=10;}// 7     /
		else if(i==10){a=9;b=8;}// 9     /
		
		a = quesload(a);
		b = quesload(b);
		
		ans1.setImageResource(a);
		ans2.setImageResource(b);
		
		ans1.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if((i==1)||(i==3)||(i==5)||(i==6)||(i==9)||(i==10))
				{
					score++;
				}
				
			    scoreshow.setText(String.valueOf(score));
				i++;
				addListenerOnButton();
				
			}
			
			
		});
		
		ans2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					//userans = ans;
				if((i==2)||(i==4)||(i==7)||(i==8))
				{
					score++;
				}
				
		    
		    scoreshow.setText(String.valueOf(score));
		    i++;
		    addListenerOnButton();
			}
		});
		
			
		}
		
		else {stopquiz();}
	}
	
	public int quesload(int i)
	{
		int imgId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("pr_"+i);
		    imgId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return imgId;
	}
	
	public void stopquiz()
	{
		String scorefinal=Integer.toString(score);
		scorefinal = scorefinal + "/10";
		String timefinal = String.valueOf(time.getText());
		int idgame = 4;
		
		Intent i = new Intent(PhraseIt.this,ScoreExer.class);
		i.putExtra("score1", scorefinal);
		i.putExtra("idgame", idgame);
		i.putExtra("time1", timefinal);
		startActivity(i);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
		
	public int randomnum()
	{
	List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
													//
	int max=10,n=1;									//
	for(int i = 1; i<=max; i++)						//
	    list.add(i);								//
													//
	Collections.shuffle(list);						//
													//
	for(int i=0; i<max; i++) {						//			
	     n = list.get(i);							//	
	}
	return n;
	}
	
		public int playmusic()
		{
		List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
														//
		int max=14,n=1;									//
		for(int i = 1; i<=max; i++)						//
		    list.add(i);								//
														//
		Collections.shuffle(list);						//
														//
		for(int i=0; i<max; i++) {						//			
		     n = list.get(i);							//	
		}												//
		
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("bg_"+n);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
		
	}
}