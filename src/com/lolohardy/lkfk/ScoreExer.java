package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ScoreExer extends Activity{

	String score, time, combo;
	int idgame;
	ImageView pic;
	TextView combo2;
	Button back,replay;
	
	public void onWindowFocusChanged (boolean hasFocus) 
	{
		pic = (ImageView) findViewById(R.id.scoreimgview);
	      	
	    super.onWindowFocusChanged(hasFocus);
	    int b = randompic();
	    pic.setBackgroundResource(b);
	      AnimationDrawable startAnimation = (AnimationDrawable) pic.getBackground();
	    if(hasFocus) {
		       startAnimation.start();
	    } else {
	        startAnimation.stop();
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		  Intent i = getIntent();
	      score = i.getStringExtra("score1");
	      time = i.getStringExtra("time1");
	      idgame = i.getIntExtra("idgame",1);
	      
	      setContentView(R.layout.scoreexer);
	      
	      
	      
		  combo2 = (TextView) findViewById(R.id.finalscore);
		  combo2.setText("Score : "+ score + "\nTime : "+ time);
		  
	      addListenerOnButton();
	      
	      
	}
	
	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		
		back = (Button) findViewById(R.id.done);
		replay = (Button) findViewById(R.id.replay);
		
		
		back.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(idgame==1)
				{
				Intent openSelectCol = new Intent("com.lolohardy.lkfk.SELECTCOL");
				startActivity(openSelectCol);
				}
				else if(idgame==2)
				{
				Intent openSelectAni = new Intent("com.lolohardy.lkfk.SELECTANI");
				startActivity(openSelectAni);	
				}
				else if(idgame==3)
				{
				Intent openSelectNum = new Intent("com.lolohardy.lkfk.SELECTNUM");
				startActivity(openSelectNum);
				}
				else if(idgame==4)
				{
				Intent openSelectPhr = new Intent("com.lolohardy.lkfk.SELECTPHR");
				startActivity(openSelectPhr);
				}
			}
		});
		
		replay.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(idgame==1)
				{
				Intent openExerCol = new Intent("com.lolohardy.lkfk.EXERCOL");
				startActivity(openExerCol);
				}
				else if(idgame==2)
				{
				Intent openSpellAni = new Intent("com.lolohardy.lkfk.SPELLANI");
				startActivity(openSpellAni);	
				}
				else if(idgame==3)
				{
				Intent openNumExer = new Intent("com.lolohardy.lkfk.TAPNUM");
				startActivity(openNumExer);
				}
				else if(idgame==4)
				{
				Intent openPhrIt = new Intent("com.lolohardy.lkfk.PHRASEIT");
				startActivity(openPhrIt);
				}
			}
		});
		
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	
	
	public int randompic()
	{
	List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
													//
	int max=7,n=1;									//
	for(int i = 1; i<=max; i++)						//
	    list.add(i);								//
													//
	Collections.shuffle(list);						//
													//
	for(int i=0; i<max; i++) {						//			
	     n = list.get(i);							//	
	}												//
		
	int picId = 0;
	try {
	    Class res = R.drawable.class;
	    Field field = null;

	    field = res.getField("ani_"+n);	
	
	    picId = field.getInt(null);
	}
	catch (Exception e) {
	    Log.e("MyTag", "Failure to get drawable id.", e);
	}
	return picId;
	
}
	
}
