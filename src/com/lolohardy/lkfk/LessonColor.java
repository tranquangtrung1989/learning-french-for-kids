package com.lolohardy.lkfk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;


public class LessonColor extends Activity {
	
	
	ImageButton prev, next, speak;
	ImageView ani,word;
	MediaPlayer bgmusic,pronounce;
	//View colflash;
	int i=1;
	
	public void onWindowFocusChanged (boolean hasFocus) 
	{
		ani = (ImageView) findViewById(R.id.imgviewAni);
		
	    super.onWindowFocusChanged(hasFocus);
		ani.setBackgroundResource(R.drawable.col_1);
		 AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
	    if(hasFocus) {
		       startAnimation.start();
	    } else {
	        startAnimation.stop();
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lesson_color);
		
		int id2 = playmusic();
				
		bgmusic = MediaPlayer.create(LessonColor.this, id2);
		bgmusic.setLooping(true);
		bgmusic.setVolume(100, 100);
		bgmusic.start();
	          		
		word = (ImageView) findViewById(R.id.imgviewWord);
		word.setBackgroundResource(R.drawable.word_1);
		addListenerOnButton();
			
	}

	public void addListenerOnButton() {
		// TODO Auto-generated method stub
		
		prev = (ImageButton) findViewById(R.id.btnprev);
		next = (ImageButton) findViewById(R.id.btnnext);
		speak = (ImageButton) findViewById(R.id.speakbut);
		
		prev.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(i==1)
				{
					i=7;
				}
				
				else
				{
					i-=1;
				}
				int col = prev(i);
				int col2 = prev2(i);
				ani.setBackgroundResource(col);
				word.setBackgroundResource(col2);
			//	colflash.setBackgroundResource(col);
				 AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
			       startAnimation.start();
			


			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
								
				if(i==7)
				{
					i=1;
				}
				else
				{
					i+=1;
				}
				int col = prev(i);
				int col2 = prev2(i);
				ani.setBackgroundResource(col);
				word.setBackgroundResource(col2);
			//	colflash.setBackgroundResource(col);
				 AnimationDrawable startAnimation = (AnimationDrawable) ani.getBackground(); 
			       startAnimation.start();
			}
		});
		
		speak.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				int sound = sound(i);
			     	
			       pronounce = MediaPlayer.create(LessonColor.this, sound);
					pronounce.setVolume(100, 100);
			        pronounce.start();
			        while(pronounce.isPlaying())
			        	{bgmusic.pause();}
			        	bgmusic.start();
			        


			}
		});
	}

	public int prev(int i)
	{
		int drawableId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("col_"+i);
		    drawableId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return drawableId;
	}
	
	public int prev2(int i)
	{
		int drawableId = 0;
		try {
		    Class res = R.drawable.class;
		    Field field = res.getField("word_"+i);
		    drawableId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return drawableId;
	}
	
	public int sound(int i)
	{
		int rawId = 0;
		try {
		    Class res = R.raw.class;
		    Field field = res.getField("col_"+i);
		    rawId = field.getInt(null);
		}
		catch (Exception e) {
		    Log.e("MyTag", "Failure to get drawable id.", e);
		}
		return rawId;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
		bgmusic.setLooping(false);
		bgmusic.stop();
		bgmusic.release();
	}
	
	public int playmusic()
	{
	List<Integer> list = new ArrayList<Integer>(); //random i generator--> random song
													//
	int max=14,n=1;									//
	for(int i = 1; i<=max; i++)						//
	    list.add(i);								//
													//
	Collections.shuffle(list);						//
													//
	for(int i=0; i<max; i++) {						//			
	     n = list.get(i);							//	
	}												//
	
	int rawId = 0;
	try {
	    Class res = R.raw.class;
	    Field field = res.getField("bg_"+n);
	    rawId = field.getInt(null);
	}
	catch (Exception e) {
	    Log.e("MyTag", "Failure to get drawable id.", e);
	}
	return rawId;
	
	}
}
