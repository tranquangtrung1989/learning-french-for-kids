package com.lolohardy.lkfk;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class Splash extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		final MediaPlayer mPlayer = MediaPlayer.create(Splash.this, R.raw.splashsound);
		mPlayer.setVolume(100, 100);
        mPlayer.start();
        
        
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(4000);
					
					
				} catch (InterruptedException e){
					e.printStackTrace();
					
				} finally {
					Intent openMainActivity = new Intent("com.lolohardy.lkfk.MAINACTIVITY");
					startActivity(openMainActivity);
					mPlayer.stop();
				}
			}
		};
		timer.start();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	
	

}
